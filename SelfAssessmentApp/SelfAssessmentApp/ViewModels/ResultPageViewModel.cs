﻿using SelfAssessmentApp.Models;
using SelfAssessmentApp.Services;
using System.ComponentModel;
using Xamarin.Forms;

namespace SelfAssessmentApp.ViewModels
{
    public class ResultPageViewModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly SelfAssessmentService assessmentService;

        public ResultPageViewModel(int[] answers)
        {
            assessmentService = new SelfAssessmentService();
            this.LoadResults(answers);

            GoToMainPageCommand = new Command(async()=> {
                await Application.Current.MainPage.Navigation.PopToRootAsync();
            });
        }

        public Command GoToMainPageCommand { get; set; }
        
        private TotalScore totalScore;
        public TotalScore TotalScore
        {
            get => totalScore;
            set
            {
                totalScore = value;
                var args = new PropertyChangedEventArgs((nameof(TotalScore)));
                PropertyChanged?.Invoke(this, args);
            }

        }

        private async void LoadResults(int[] answers)
        {
            TotalScore = await assessmentService.PostAnswers(answers);
        }

    }
}
