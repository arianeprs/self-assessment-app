﻿using SelfAssessmentApp.Configs;
using SelfAssessmentApp.Models;
using SelfAssessmentApp.Services;
using SelfAssessmentApp.Views;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;

namespace SelfAssessmentApp.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private readonly List<Question> questions;
        private readonly int[] answers;
        private readonly SelfAssessmentService assessmentService;

        public event PropertyChangedEventHandler PropertyChanged;

        public MainPageViewModel()
        {
            assessmentService = new SelfAssessmentService();
            questions = new List<Question>();
            answers = new int[Config.MaxQuestionNumber];

            this.InitAssessmentForm();

            NextQuestionCommand = new Command(() =>
            {
                if (CurrentQuestionNumber < Config.MaxQuestionNumber &&
                answers[CurrentQuestionNumber - 1] != -1)
                {
                    CurrentQuestionNumber++;
                    SelectedRadioButtonIndex = answers[CurrentQuestionNumber - 1];
                    this.LoadQuestion(CurrentQuestionNumber);
                }

                if (CurrentQuestionNumber == Config.MaxQuestionNumber)
                {
                    var resultVM = new ResultPageViewModel(answers);
                    var resultPage = new ResultPage
                    {
                        BindingContext = resultVM
                    };

                    if (!answers.Contains(-1))
                    {
                        Application.Current.MainPage.Navigation.PushAsync(resultPage);
                    }
                }
            });

            PreviousQuestionCommand = new Command(() =>
            {
                if (CurrentQuestionNumber > Config.MinQuestionNumber)
                {
                    CurrentQuestionNumber--;
                    SelectedRadioButtonIndex = answers[CurrentQuestionNumber - 1];
                    this.LoadQuestion(CurrentQuestionNumber);
                }
            });

            RestartTestCommand = new Command(() =>
            {
                this.InitAssessmentForm();
            });
        }

        private int selectedRadioButtonIndex;
        public int SelectedRadioButtonIndex {
            get => selectedRadioButtonIndex;
            set {
                selectedRadioButtonIndex = value;
                answers[CurrentQuestionNumber - 1] = value;
                var args = new PropertyChangedEventArgs((nameof(SelectedRadioButtonIndex)));
                PropertyChanged?.Invoke(this, args);
            }
        }

        private int currentQuestionNumber;
        public int CurrentQuestionNumber {
            get => currentQuestionNumber;

            set {
                currentQuestionNumber = value;
                var args = new PropertyChangedEventArgs((nameof(CurrentQuestionNumber)));
                PropertyChanged?.Invoke(this, args);
            }
        }

        private Question currentQuestion;
        public Question CurrentQuestion {
            get => currentQuestion;

            set {
                currentQuestion = value;
                var args = new PropertyChangedEventArgs((nameof(CurrentQuestion)));
                PropertyChanged?.Invoke(this, args);
            }

        }

        public Command PreviousQuestionCommand { get; set; }
        public Command NextQuestionCommand { get; set; }
        public Command RestartTestCommand { get; set; }
        
        private void InitAssessmentForm() { 
            CurrentQuestionNumber = Config.MinQuestionNumber;
            SelectedRadioButtonIndex = -1;
            this.InitAnswers();
            this.LoadQuestion(CurrentQuestionNumber);
        }

        private void InitAnswers()
        {
            for (int i = 0; i < answers.Length; i++) {
                answers[i] = -1;
            }
        }

        private async void LoadQuestion(int questionNumber)
        {
            if (this.questions.Count < questionNumber) {
                CurrentQuestion = await assessmentService.GetQuestion(
                    questionNumber.ToString());

                this.questions.Insert(
                    questionNumber - 1, 
                    CurrentQuestion);
            }

            else {
                CurrentQuestion = this.questions[questionNumber - 1];
            }
        }

    }
}
