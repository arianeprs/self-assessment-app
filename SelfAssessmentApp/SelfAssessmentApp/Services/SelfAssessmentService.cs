﻿using Newtonsoft.Json;
using SelfAssessmentApp.Configs;
using SelfAssessmentApp.Models;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace SelfAssessmentApp.Services
{
    public class SelfAssessmentService : BaseApiService
    {

        public SelfAssessmentService() : base()
        { }

        public async Task<Question> GetQuestion(
            string questionNumber
        ) {
            Question question = new Question();

            try
            {
                HttpResponseMessage apiResponse = await HttpClient.GetAsync(
                    this.Url(
                        ApiRoutes.Question,
                        questionNumber
                        )
                    );

                if (apiResponse.IsSuccessStatusCode)
                {
                    string content = await apiResponse.Content.ReadAsStringAsync();
                    question = JsonConvert.DeserializeObject<Question>(content);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error {e.Message}");
            }

            return question;
        }

        public async Task<TotalScore> PostAnswers(
            int[] answers
        ) {
            var answersJson = JsonConvert.SerializeObject(new Answers(answers));
            var content = this.StringContent(answersJson);
            var totalScore = new TotalScore();

            try
            {
                HttpResponseMessage apiResponse = await HttpClient.PostAsync(
                    this.Url(
                        ApiRoutes.Answer
                        ),
                    content
                    );

                if (apiResponse.IsSuccessStatusCode)
                {
                    string response = await apiResponse.Content.ReadAsStringAsync();
                    totalScore = JsonConvert.DeserializeObject<TotalScore>(response);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Error {e.Message}");
            }

            return totalScore;
        }
    }
}
