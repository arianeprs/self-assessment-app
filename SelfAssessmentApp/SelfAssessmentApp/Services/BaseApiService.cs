﻿using SelfAssessmentApp.Configs;
using System.Net.Http;

namespace SelfAssessmentApp.Services
{
    public abstract class BaseApiService
    {
        protected HttpClient HttpClient { get; }

        public BaseApiService()
        {
            HttpClient = new HttpClient();
        }

        protected string Url(
            string url,
            string param = "")
        {
            return $"{ApiRoutes.BaseUrl}/api/{url}/{param}";
        }

        protected StringContent StringContent(string content)
        {
            return new StringContent(
                content, 
                Config.HttpEncoding, 
                Config.HttpMediaType);
        }

    }
}
