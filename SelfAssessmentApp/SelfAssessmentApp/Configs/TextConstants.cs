﻿namespace SelfAssessmentApp.Configs
{
    public class TextConstants
    {
        public readonly static string Previous = "Previous";
        public readonly static string Next = "Next";
        public readonly static string StartOver = "Start Over";
        public readonly static string GoBack = "Go Back";
    }
}
