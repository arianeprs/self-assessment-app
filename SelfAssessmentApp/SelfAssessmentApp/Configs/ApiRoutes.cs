﻿namespace SelfAssessmentApp.Configs
{
    public class ApiRoutes
    {
        public readonly static string BaseUrl = "http://flowns-app-test.herokuapp.com";
        public readonly static string Question = "question";
        public readonly static string Answer = "answer";
    }
}
