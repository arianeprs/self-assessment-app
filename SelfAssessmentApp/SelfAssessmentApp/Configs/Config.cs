﻿using System.Text;

namespace SelfAssessmentApp.Configs
{
    public class Config
    {
        public static readonly int MinQuestionNumber = 1;
        public static readonly int MaxQuestionNumber = 9;
        public static readonly Encoding HttpEncoding = Encoding.UTF8;
        public static readonly string HttpMediaType = "application/json";
    }
}
