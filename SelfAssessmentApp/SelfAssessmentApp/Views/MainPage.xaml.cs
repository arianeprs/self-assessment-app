﻿using SelfAssessmentApp.Models;
using SelfAssessmentApp.Services;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace SelfAssessmentApp
{

    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

    }
}
