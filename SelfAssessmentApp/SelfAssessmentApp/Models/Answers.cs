﻿using Newtonsoft.Json;

namespace SelfAssessmentApp.Models
{
    public class Answers
    {
        [JsonProperty("answers")]
        public int[] Array { get; set; }

        public Answers(int[] array)
        {
            Array = array;
        }
    }
}