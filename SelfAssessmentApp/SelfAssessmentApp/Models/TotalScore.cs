﻿using Newtonsoft.Json;

namespace SelfAssessmentApp.Models
{
    public class TotalScore
    {
        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("severity")]
        public string Severity { get; set; }
    }
}
