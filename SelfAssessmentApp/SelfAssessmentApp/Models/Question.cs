﻿using Newtonsoft.Json;

namespace SelfAssessmentApp.Models
{
    public class Question
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "rsp-1")]
        public Response Rsp1 { get; set; }

        [JsonProperty(PropertyName = "rsp-2")]
        public Response Rsp2 { get; set; }

        [JsonProperty(PropertyName = "rsp-3")]
        public Response Rsp3 { get; set; }

        [JsonProperty(PropertyName = "rsp-4")]
        public Response Rsp4 { get; set; }
    }
}
